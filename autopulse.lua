#!/bin/luajit

-- Load bash functions
local sh = require('sh')
local xdo = sh.command("xdotool")
local pulse = sh.command("pacmd")
local xrandr = sh.command("xrandr")
local sleep = sh.command("sleep")

----------------------
-- Start Configuration
local displayDefinitions = {
	{name="DVI-0", output="ALC892 Analog"},
	{name="DVI-1", output="ALC892 Analog"},
	{name="DisplayPort-1", output="HDMI 0"}
}

local windowDefinitions = {
	-- Firefox Pages --
	---- All Firefox windows have "Navigator" as their class, and contain "Mozilla Firefox" in their name.
	-- {class="Navigator", name="Netflix"},				-- Netflix
	{class="Navigator", name="Youtube"},				-- Youtube
	-- {class="Navigator", name="Spotify Web Player"},	-- Spotify

	-- Applications --
	{class="Spotify"} -- Spotify, we don't want to specify a name, because the window will change its name based on the currently playing track
	-- {
}

-- End Configuration
--------------------

-- Find display geometry using xrandr
local function getDisplays()
	local displays = {}
	local displayInfo = sh.split(xrandr())
	for _, line in pairs(displayInfo) do
		if string.find(line, " connected ") then
			local _, _, name = string.find(line, "(%a+%-%d+) ")
			local _, _, width, height, x, y = string.find(line, "(%d+)x(%d+)+(%d+)+(%d+)")
			table.insert(displays, {name=name, x=tonumber(x), y=tonumber(y), width=tonumber(width), height=tonumber(height)})
		end
	end

	return displays
end

-- Find all visible windows matching windowDefinitions
local function getWindows()
	local windows = {}
	for _, window in pairs(windowDefinitions) do
		if window.class and window.name then
			local classList = sh.split(xdo('search --classname --onlyvisible', '"'..window.class..'"'))
			local nameList = sh.split(xdo('search --name', '"'..window.name..'"'))
			for _, name in pairs(nameList) do
				for _, class in pairs(classList) do
					if name == class then
						local duplicate = false
						for _, match in pairs(windows) do
							if name == match then
								dupliate = true
								break
							end
						end
						if duplicate == false then
							table.insert(windows, name)
						end
					end
				end
			end
		elseif window.class then
			local classList = sh.split(xdo('search --classname --onlyvisible', '"'..window.class..'"'))
			local duplicate = false
			for _, class in pairs(classList) do
				for _, match in pairs(windows) do
					if class == match then
						duplicate = true
						break
					end
				end
				if duplicate == false then
					table.insert(windows, class)
				end
			end
		elseif window.name then
			local nameList = sh.split(xdo('search --name --onlyvisible', '"'..window.name..'"'))
			local duplicate = false
			for _, name in pairs(nameList) do
				for _, match in pairs(windows) do
					if name == match then
						duplicate = true
						break
					end
				end
				if duplicate == false then
					table.insert(windows, name)
				end
			end
		end
	end

	return windows
end

-- Get displays, but only once on startup because xrandr freezes the screen
local displays = getDisplays()
while true do
	local windows = getWindows()
	local done = {}
	for _, xid in pairs(windows) do
		local name = tostring(xdo('getwindowname', xid))
		local pid = tostring(xdo('getwindowpid', xid))
		local geometry = tostring(xdo('getwindowgeometry', xid))
		local _, _, x, y = geometry:find("Position: ([+-]?%d+),([+-]?%d+)")
		local _, _, width, height = geometry:find("Geometry: (%d+)x(%d+)")
		local midX = tonumber(x + (width/2))
		local midY = tonumber(y + (height/2))

		-- Figure out which display the window is on
		for _, display in pairs(displays) do
			if (midX > display.x) and (midX < display.x + display.width) then
				if (midY > display.y) and (midY < display.y + display.height) then
					for _, definition in pairs(displayDefinitions) do
						if display.name == definition.name then
							-- Find the correct pulseaudio sink based on the display
							local pulseOutputs = sh.split(pulse("list-sinks"))
							local outputIndex
							local outputId
							for _, line in pairs(pulseOutputs) do
								if string.find(line, "index: ") then
									local _, _, index = string.find(line, "index:%s(%d+)")
									outputIndex = index
								end
								if string.find(line, "alsa.id = ") then
									local _, _, id = string.find(line, 'alsa.id%s=%s"(.+)"')
									if tostring(id) == definition.output then
										outputId = id
										break
									end
								end
							end

							-- Find the correct pulseaudio sink-input index using process id
							local pulseInputs = sh.split(pulse("list-sink-inputs"))
							local inputIndex
							local inputId
							for _, line in pairs(pulseInputs) do
								if string.find(line, "index:%s") then
									local _, _, index = string.find(line, "index:%s(%d+)")
									inputIndex = index
								end
								if string.find(line, "application.process.id = ") then
									local _, _, id = string.find(line, 'application.process.id%s=%s"(.+)"')
									if id == pid then
										inputId = id
										break
									end
								end
							end
							-- Finally, change the output device!!! Hope this works :]
							if inputId and inputIndex and outputId and outputIndex then
								local skip = false
								for _, process in pairs(done) do
									print(process)
									if process == pid then
										print(pid)
										skip = true
									end
								end
								if skip == false then
									print(name, midX, midY, pid)
									pulse("move-sink-input", tostring(inputIndex), tostring(outputIndex))
									table.insert(done, pid)
								end
							end
						end
					end
				end
			end
		end
	end
	sleep(0.5)
end
